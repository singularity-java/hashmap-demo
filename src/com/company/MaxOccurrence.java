package com.company;

import java.util.HashMap;
import java.util.Map;

public class MaxOccurrence {
    public static int findMaxOccurrence(String text) {
        HashMap<Character, Integer> hmap = new HashMap<>();
        for(Character ch: text.toCharArray()) {
            if(!hmap.containsKey(ch)) {
                hmap.put(ch, 1);
            } else {
                hmap.put(ch, hmap.get(ch)+1);
            }
        }

        System.out.println(hmap);

        int max = Integer.MIN_VALUE;
        for(Map.Entry<Character, Integer> entry: hmap.entrySet()) {
            if(max < entry.getValue()) {
                max = entry.getValue();
            }
        }
        return max;
    }

    public static void main(String[] args) {
        int occurrence = findMaxOccurrence("banana");
        System.out.println(occurrence);
    }
}

package com.company;

import java.util.HashMap;
import java.util.Map;

public class Main extends Object {

    public static void main(String[] args) {
	// write your code here
        String str = "helloworld";
        int hash = str.hashCode();
        System.out.println("String: " + str);
        System.out.println("hash: " + hash);

        HashMap<String, String> students = new HashMap<>();
        students.put("001000001", "Makhambet Torezhan");
        students.put("001000002", "Alice Smith");
        students.put("001000003", "Bob Smith");

        students.remove("001000001");

        for(Map.Entry<String, String> entry: students.entrySet()) {
            System.out.println("Id: " + entry.getKey());
            System.out.println("Name: " + entry.getValue());
        }

        System.out.println("get function: " + students.get("001000001"));
        //students.clear();
        if(students.isEmpty()) {
            System.out.println("Students is Empty");
        } else {
            System.out.println("Students not empty");
        }

        if(students.containsKey("001000001")) {
            System.out.println("Does contain 001000001");
        } else {
            System.out.println("Does not contain 001000001");
        }

        HashMap<String, String> clone = (HashMap<String, String>) students.clone();
        System.out.println("Size: " + students.size());
    }
}
